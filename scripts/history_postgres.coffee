# Description:
#   "Simple history script using sqlite"
#
# Dependencies:
#   "sqlite3": "2.1.10"
#
# Configuration:
#   None
#
# Commands:
#   None
#
# URLs:
#   GET /hubot/say?message=<message>[&room=<room>&type=<type]
#
# Author:
#   pboos
pg          = require 'pg'
dateFormat  = require 'dateformat'

db = process.env.HEROKU_POSTGRESQL_BLUE_URL
client = new pg.Client db
client.connect()
client.query("CREATE TABLE IF NOT EXISTS history (
                id SERIAL,
                time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                username TEXT,
                message TEXT
              )")

module.exports = (robot) ->
  # setup own message interceptor
  oldSay = robot.adapter.bot.say
  robot.adapter.bot.say = (target, str) ->
    oldSay.apply robot.adapter.bot, [target, str]
    client.query("INSERT INTO history (username, message) VALUES ($1,$2)",
                 [robot.name, str])

  robot.respond /history/i, (msg) ->
    msg.send "Find the history here: " + process.env.HEROKU_URL + "/hubot/history"

  robot.hear /(.*)/i, (msg) ->
    client.query("DELETE FROM history WHERE id IN ( SELECT id FROM history ORDER BY time DESC OFFSET 5000 )");
    client.query("INSERT INTO history (username, message) VALUES ($1,$2)",
                 [msg.message.user.name, msg.message.text])
    # TODO maybe remove all items that are too old?

  robot.router.set 'view engine', 'jade'
  robot.router.set 'views', __dirname + '/../views'
  # robot.router.use express.staticProvider(__dirname + '/../public')
  robot.router.get "/hubot/history", (req, res) ->
    res.type("html")
    messages = []
    query = client.query("SELECT * FROM history ORDER BY id DESC LIMIT 1000")
    query.on 'row', (row) ->
        format = "mm/dd HH:MM:ss"
        format = "HH:MM:ss" if dateFormat(row.date, "yyyy-mm-dd") == dateFormat(new Date(), "yyyy-mm-dd")
        date = new Date(row.time + " GMT")
        minus = date.getTimezoneOffset() * 60000
        plus = 9 * 60 * 60 * 1000
        date.setTime(date.getTime() + minus + plus) # make it JST
        messages.push {name: row.username, message: row.message, date: dateFormat(date, format)}
    query.on 'end', () ->
        res.render('main', {messages:messages, channel: 'goodolgaijins'})
